package com.example.baselib;

import com.example.baselib.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseLibApplication implements CommandLineRunner {

    @Autowired
    private MyService myService;

    public static void main(String[] args) {
        SpringApplication.run(BaseLibApplication.class, args);
    }
    //access command line arguments

    @Override
    public void run(String... args) throws Exception {

        //do something
        myService.log();
    }
}
