package com.example.baselib.service;

import com.example.baselib.config.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MyService {
    @Autowired
    Config config;

    public void log(){
        log.info(">>>>>>Prop value " + config.getMyProp());
    }
}
