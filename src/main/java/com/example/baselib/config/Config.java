package com.example.baselib.config;

import com.example.baselib.service.MyService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@Getter
public class Config {
    @Value("${myProp}")
    private String myProp;

    @Bean(name = "myService") //register bean to be available in client modules
    public MyService getMyService() {
        return new MyService();
    }


}
